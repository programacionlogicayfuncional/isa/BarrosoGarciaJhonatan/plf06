(ns plf06.core
  (:gen-class))


;Función
(defn rot13
  [s] (let [alfabeto-minusculas "aábcdeéfghiíjklmnñoópqrstuúüvwxyz"
            alfabeto-mayusculas "AÁBCDEÉFGHIÍJKLMNÑOÓPQRSTUÚÜVWXYZ"
            simbolos "01234!\"#$%&'()*+,-./:;<=>?@[\\]^_`{|}~56789"
            h (fn [s m c]
               (if (>= (- (dec (count s)) 13) (m c))
                 (+ (m c) 13)
                 (- (+ (m c) 13) (count s))))
            ;Obtener el caracter equivalente
            k (fn [s c] (get s (h s (zipmap s (range 0 (count s))) c)))
            ;Crear un mapa, clave: caracter key: equivalente 
            t (fn [xs]
                    (letfn [(g [ys zs]
                              (if (nil? (first ys))
                                zs
                                (g (rest ys) (conj zs [(first ys) (k xs (first ys))]))))]
                      (g xs {})))
            
            
            tabla-simbolos (conj (t alfabeto-minusculas) (t alfabeto-mayusculas) (t simbolos))
            f (fn [] (apply str (replace tabla-simbolos s)))] (f)))


(rot13 "Canción #72")
(rot13 "c AN CIÓN # 7 2")
(rot13 "hola")
(rot13 "canción")
(rot13 "#72")



;Lófica que se planteó 
(count "aábcdeéfghiíjklmnñoópqrstuúüvwxyz")
(get "aábcdeéfghiíjklmnñoópqrstuúüvwxyz" (if (>= 19 ((zipmap "aábcdeéfghiíjklmnñoópqrstuúüvwxyz" (range 0 33)) \z ))
                                          (+((zipmap "aábcdeéfghiíjklmnñoópqrstuúüvwxyz" (range 0 33)) \z)13)
                                           (- (+ ((zipmap "aábcdeéfghiíjklmnñoópqrstuúüvwxyz" (range 0 33)) \z) 13) 33)))

(if (>= 19 ((zipmap "aábcdeéfghiíjklmnñoópqrstuúüvwxyz" (range 0 33)) \z ))
                                          (+((zipmap "aábcdeéfghiíjklmnñoópqrstuúüvwxyz" (range 0 33)) \z)13)
                                           (- (+ ((zipmap "aábcdeéfghiíjklmnñoópqrstuúüvwxyz" (range 0 33)) \z) 13) 33))
({:a 1 :b 2} :a)
(defn equivalencia
  [s c]
  (let [f (fn [m]
            (if (>= (- (dec (count s)) 13) (m c))
            (+ (m c) 13)
            (- (+ (m c) 13) (count s))))] (get s(f (zipmap s (range 0 (count s)))))))

(equivalencia "aábcdeéfghiíjklmnñoópqrstuúüvwxyz" \z)

; Convierte un vector en un mapa
(def vector-to-mapa (fn [xs]
             (letfn [(g [ys zs]
                       (if (nil? (first ys))
                         zs
                         (g (rest ys) (conj zs [(first ys) (equivalencia xs (first ys))]))))]
               (g xs {}))))

(vector-to-mapa "aábcdeéfghiíjklmnñoópqrstuúüvwxyz")


(count "AÁBCDEÉFGHIÍJKLMNÑOÓPQRSTUÚÜVWXYZ")
(count "01234!\"#$%&'()*+,-./:;<=>?@[\\]^_`{|}~56789")




(defn -main
  [& args]
  (if (empty? args)
    (println "Error, es necesario ingresar algún argumento.")
    (println (rot13 (apply str args)))))
